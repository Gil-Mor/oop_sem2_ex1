﻿
1.----------- README -----------------

2. ---------- STUDENT --------------- 
Ex1 OOP Sem 2
Gil Mor גיל מור 300581030

3.-------------- THE EXERCISE -----------------
Painter Program.
The user can choose from predefined shapes (sqaure, traingles etc..)
or draw Polygons. 
The shapes can be rotated and scaled.
There's also 'one level undo button 

(There is only one level undo in this program so pressing
the undo twice undoes the last undo (redo)).

and delete button that deletes chosen
shapes. The user can chose a shape to operate on with the arrow keys.

4.--------------- DESIGN -----------------

---TECHNICAL DESICIONS

1. You can choose shapes by clicking the choice buttons
   or by using the keyboard's arrow keys.


2. When choosing to draw a shape (or a square at the begining)
   You'll draw the same type of shape until you choose another shape
   or perform some other action. 


---SHAPES
Shape is the base class for all shapes that can be drawn.
A shape can be visible and chosen.
The Visible attribute is used for temporary delete of shapes
so that the undo can restore them easily and in their original positions 
in the shapes array in the controller.
If a Shape is Chosen then the transformations will work on that shape.

There are ---TRANSFORMABLE SHAPES that implement a rotate and scale operations.
These shapes keep two variables "rotatation" and "scaleFactor".

There are Static Shapes which don't implement any additional behaviour..
It's just for the name..
Only the Polygons are static shapes.

The Open Polygon and Closed polygon are only different in the way
they add the last vertex of the polygon.
The Open Polygon adds it where the user presses with the mouse
where the closed Polygon connencts it to the first vertex ignoring
the user's last click.

the House Shape is made from a Square Shape and an EquilateralTriangle shape.
The Star Of David is made from two EquilateralTriangle shapes.

There are Equilateral Triangle and Isosceles Triangle.
they are derived from Triangle.
The only differnce between them is the Initialization of the 
first positions of the vertices.


---COMMANDS
There are two types of commands.
One is The ControllerCommands which call methods in the controller.
The drawing buttons tell the Controller which Shape to add to the Shapes array.
Another ControllerCommand is the ChooseShape which is used by the ChooseShapeButton
to choose shapes on the board from older to newer or newer to older.

The state word comes from the need for "DrawingPolygon" state.
When Drawing a Polygon the program runs into another sfml loop
that adds vertices to the polygon with every click until a left click
which draws the last vertex.

The other type of commands is the "Transform Commands".
TransformCommand work on chosen shapes.
Each "transform Command" is a member of the appropriate ---BUTTON---.
When "Transformation Buttons" are pressed they send to the Controller
Their "Command" member. the Controller calls the "action" method of
these commands with the currently chosen shapes (if there are any).
Each command checks the type of the given shape Internaly.
This gives the freedom to use these commands on all shapes generically
but the Commands themselves decide whether t ooperate
on the shapa (For example "HouseMoveCommand" operates only on house shapes).

The TransformCommands implement an "action" method and an "undo"
method that accapt a Shape& and perform the transformation on that
shape. If the transformation is scale or rotate then the Command
will try to dynamic_cast the Shape to "transformableShape"
the "DeleteCommand" is also a TransformCommand but it operates on ALL
Shapes.

The Controller saves the last TransformCommand that was performed
and keeps track of the "undo" state (undo or undo to the undo).

IN GENERAL the Controller keeps track and syncs between actions and their undos
(drawing and Transformations...)


---BUTTONS
There are the Transformation Buttons which have "transfformCommand" member..
There are "Drawing Buttons" which get a "Controller::State" on construction
and change the controller state to that state (which tells the Controller 
which shape to draw now..)

Besides there are Buttons which call special methods in the Controller
like the "NewButton" which opens a new page, 
"ColorButton" which changes the color for new shapes to be drawn
and "SelectSimilarButton" which select all shapes of the same type
of th currently chosen shape.


---CONTROLLER
The Controller runs the main SFML loop until the user closes the window.
In each loop the Controller pools events and gets the mouse location.
If the mouse is above the "Menu" then the Controller sends
the Menu the current event and the mouse location and the menu
handles hovering above buttons and clicking on buttons.

If the mouse is over the drawing board then if there is a click
and we are in some drawing mode then the Controller will
add that Shape in the Mouse current location
or in the special case if the Polygons we'll get into
another loop until the user clicks the left mouse button
and draws the last vertex.

The Controller keeps all the shapes in a vector of unique_ptrs.
The Controller keeps track of the last operations that were performed,
on the Selections etc..


---SHAPES DELETION
The "Delete Button" has a "Delete Command" member.
Delete Command set the shape to be invisible (action) or back to visible (undo)
Then It's easy to delete even a lot of shapes and then restore 
them in their previous position (in the array).
After a different action is made the Controoler willdelete these shapes
from the array and will shrink it.

---TEXTURE MANAGER
Singelton that loads the Menu textures.
It's more conviniant to load all the textures in one place.

---INFO WINDOW
Displays some information about the drawn shapes.
Its a member of the Controller.
The Controller updates the number of different shapes (simle, complex, Polygons)
after every shape addition and deletion.
The total area and diameter are calculated before each draw
and sent to the InfoWindow.


---WINDOW PROPORTIES
A header file with the SFML positions and sizes of the board, the Menu
and the Info Window.

5.--------------- LIST OF FILES -----------------

* main.cpp
the main function

* windowProporties.h
header file with SFML sizes and positoins  of the menu, board, and Info window.

* Controller.cpp Controller.h
source and header of the Controller class

* InfoWindow.cpp InfoWindow.h
  Displays information about the shapes on the drawing board.

* Menu.cpp Menu.h
  A menu. Holds a vector of buttons.
  Handles event he gets from the controller.


--- SHAPES ---

* Shape.cpp Shape.h
  source and header of Shape class.
  The base class for all Shapes tha tare drawn on the board.


* TransformableShape.cpp / .h
  base class for all Transformable shapes.
  (shapes that can rotate and be scaled).
 
* Square.cpp / .h
  A square.
  
* Triangle.cpp / .h
  Abstract base class for all non triangle shapes.

* EquilateralTriangle.cpp / .h
  EquilateralTriangle.

* IsoscelesTriangle.cpp / .h
  IsoscelesTriangle.

* House.cpp / .h
  A house holds a Square and an EquilateralTriangle.

* StarOfDavid.cpp / .h
  holds two EquilateralTriangles.


* StaticShape.cpp / .h
  base class for all non Transformable shapes.
  (shapes that can't rotate and be scaled).

* Polygon.cpp / .h
  base class for polygons.
  Implements all the polygons behavior except appending the last vertex.

* ClosedPoly.cpp / .h
  Polygon that it's last vertex is ON the first vertex.

* OpenPoly.cpp / .h
  Polygon that it's last vertex is were the user draws it.


--- BUTTONS ---

* Button.cpp Button.h
  source and header of Button class
  Button is the base class of all buttons.
  Menu has a vector of Buttons*. 
  Button has a pure virtual method 'action'
  that's implemented differently in every button.

* DrawingButton.cpp DrawingButton.h
  source and header of DrawingButton class.
  set the controller in differnet drawing modes.

* DeleteButtun.cpp DeleteButtun.h
  Deletes the Chosen Shapes if There are any.
  It actuaaly sets the shapesto be invisible and when the next action is
  performed the Controller deletes these shapes.

* NewButton.cpp / .h
  Start a new drawing board.

* RotateButton.cpp / .h
  Holds a "RotateCommand".
  rotates a TransformableShape if one is chosen.

* HouseMoveButotn.cpp / .h
  Holds a "HouseMoveCommand".
  moves the house shape down 10 pixles.

* EnlargeButton.cpp / .h
  Holds a "EnlargeCommand".
  enlarges a TransformableShape if one is chosen.

* ShrinkButton Holds a "ShrinkCommand".
  shrinks a TransformableShape if one is chosen.

* UndoButton.cpp / .h
  tells the Controller to undo the last operation.
  There is only one level undo in this program so pressing
  the undo twice undoes the last undo (redo).

* ColorButton.cpp / .h
  changes the color of future shapes.

* SelectSimilarButton.cpp / .h
  selects shapes of the same type as the currently chosen shape
  (if there is one).

* ChooseshapeButton.cpp / .h
  There are two of these. They both choose shapes in a circular
  manner in order of creation. 
  There's one to choose "forward" from older to newer shapes
  and there one to choose "Backward".

* DrawingButton.cpp DraiwngButton.h
  Holds a Controller::State member given to him in the time 
  of construction.
  Tells the controller which shape to draw.


--- COMMANDS ---

* StateCommands.cpp StateCommands.h
  Encapsulates calls to Controller::setState method.
  used by the Drawing Buttons.

* TransformCommand.cpp TransformCommand.h
  base class for TransformCommands.
  Gets a Shape& to action and undo methods.
  perform some operation on the shape(action) 
  or reverses that operation restoring (undo).

* DeleteCommand.cpp DeleteCommand.h
  Derived from TransformCommand.
  Gets a Shape& to action and undo methods
  that set The shape to invisible(action) or back to visible(undo).

* EnlargeCommand.cpp EnlargeCommand.h
  Derived from TransformCommand.
  Gets a Shape& to action and undo methods.
  enlarge the Shape in a constant factor(action) 
  or shrink it back to its former size(undo).


* ShrinkCommand.cpp ShrinkCommand.h
  Derived from TransformCommand.
  Gets a Shape& to action and undo methods.
  shrinks the Shape in a constant factor(action) 
  or enlarges it back to its former size(undo).

* RotateCommand.cpp RotateCommand.h
  Derived from TransformCommand.
  Gets a Shape& to action and undo methods.
  rotates the Shape in a constant angle(action) 
  and rotates it back to its former angle(undo).

  * HouseMoveCommandCommand.cpp HouseMoveCommandCommand.h
  Derived from TransformCommand.
  Operates only on the House shape
  Moves the house down 10 pixles(action) 
  Moves the house back to it's previous position(undo).

--- TEXTURE MANAGER

* TextureManager.cpp / .h
  Singelton that Loads all the program textures. 
 


6.--------------- DATA STRUCTURES -----------------

1. TextureManager is a Singelton.

2. Commands Encapsulates calls to Controller methods
and operation on shpaes that the controller can save and 
reuse in undo.

7. --------------- NOTABLE ALGORITHMS -----------------


8. ------------------- KNOWN BUGS ---------------------


