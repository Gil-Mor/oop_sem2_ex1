#include "Button.h"
#include "Controller.h"


Button::Button(Controller& controller, sf::Vector2f& origin)
    :_controller(controller), _inFocus(false), _chosen(false)
{
    _shape.setSize(BUTTON_SIZE);
    _shape.setPosition(origin);
    _shape.setFillColor(BUTTON_NO_FOCUS_COLOR);
}


Button::~Button()
{}

void Button::setInFocus(bool f)
{
    if (getChosen())
        return;
    if (f)
    {
        _shape.setFillColor(BUTTON_FOCUS_COLOR);
    }
    else
    {
        _shape.setFillColor(BUTTON_NO_FOCUS_COLOR);
    }
    _inFocus = f;
}

void Button::setChosen(bool c)
{
    _chosen = c;
    if (c)
        _shape.setFillColor(BUTTON_PRESSED_COLOR);
    else
        _shape.setFillColor(BUTTON_NO_FOCUS_COLOR);
}

bool Button::mouseOver(const sf::Vector2f& mouse) const
{
    return _shape.getGlobalBounds().contains(mouse);
}

bool Button::getInFocus() const
{
    return _inFocus;
}

bool Button::getChosen() const
{
    return _chosen;
}

void Button::draw(sf::RenderWindow& window) const
{
    window.draw(_shape);
}


