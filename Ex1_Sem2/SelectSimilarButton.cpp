#include "SelectSimilarButton.h"
#include "Controller.h"
#include "TextureManager.h"

SelectSimilarButton::SelectSimilarButton(Controller& c, sf::Vector2f origin)
    : Button(c, origin)
{
    _shape.setTexture(&(TextureManager::getInstance().getMenuTexture(MenuTextures::SelectSimilars)));
}


SelectSimilarButton::~SelectSimilarButton()
{
}

void SelectSimilarButton::action()
{
    _controller.selectSimilars();
}
