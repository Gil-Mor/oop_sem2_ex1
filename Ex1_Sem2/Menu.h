#pragma once
#include "Button.h"

#include <vector>
#include <memory>
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>

using std::vector;
using std::unique_ptr;

class Controller;
class StateCommands;

class Menu 
{
public:

    Menu(Controller& controller);

    virtual ~Menu();

    /* Iterate through the buttons and activate action
    In the button that was clicked.*/
    void run(sf::RenderWindow& window);

    void handleEvent(const sf::RenderWindow& window, 
        const sf::Event event);

    void draw(sf::RenderWindow& window) const;

    /* Returns true if the mouse is over this object RectangleShape.
    the mouse coordinatesare given in 'world' coordinates -
    from window.mapPixelToCoords function.
    That's why Vector2f and not 2i. */
    virtual bool mouseOver(const sf::Vector2f& mouse) const;

    void handleEvent(const sf::Event& e, const sf::Vector2f mouse);

private:

    /* vector of menu buttons. */
    vector<unique_ptr<Button>> _buttons;

    sf::RectangleShape _shape;
    void setShape();

    StateCommands& _stateCommands;

    //============ HANDLE EVENTS ====================

    void handleMouseMove(const sf::Vector2f mouse);
    void handleMousePress(const sf::Vector2f mouse);

    bool buttonWasPressed() const;


    //======== INITIALIZE BUTTONS ===============
    void setButtons(Controller& controller);

    void Menu::advancePosition(sf::Vector2f& pos,
        const sf::Vector2f& gap, unsigned int& alreadyInRow);

    
};


