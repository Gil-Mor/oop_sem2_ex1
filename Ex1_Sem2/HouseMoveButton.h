#pragma once
#include "Button.h"
#include "HouseMoveCommand.h"

class Controller;

class HouseMoveButton :
    public Button
{
public:
    HouseMoveButton(Controller& controller, sf::Vector2f& origin);
    virtual ~HouseMoveButton();

    void action();

private:
    HouseMoveCommand _moveCommand;
};

