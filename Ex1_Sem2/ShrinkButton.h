#pragma once
#include "Button.h"
#include "ShrinkCommand.h"
class ShrinkButton :
    public Button
{
public:
    ShrinkButton(Controller& c, sf::Vector2f origin);
    virtual ~ShrinkButton();

    void action();

private:
    ShrinkCommand _shrinkCommand;
};

