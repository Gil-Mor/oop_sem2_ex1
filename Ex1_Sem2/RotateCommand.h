#pragma once
#include "TransformCommand.h"

class RotateCommand : public TransformCommand
{
public:
    RotateCommand();
    ~RotateCommand();


    void action(Shape& shape);
    void undo(Shape& shape);

};
