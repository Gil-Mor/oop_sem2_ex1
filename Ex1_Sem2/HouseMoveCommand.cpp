#include "HouseMoveCommand.h"
#include "Shape.h"
#include "House.h"

const float ACTION_FACTOR = 10;
const float UNDO_FACTOR = -10;

HouseMoveCommand::HouseMoveCommand()
{
}


HouseMoveCommand::~HouseMoveCommand()
{}

void HouseMoveCommand::action(Shape& shape)
{
    if (House* h = dynamic_cast<House*>(&shape))
        h->move10P(ACTION_FACTOR);
}

void HouseMoveCommand::undo(Shape& shape)
{
    if (House* h = dynamic_cast<House*>(&shape))
        h->move10P(UNDO_FACTOR);
}
