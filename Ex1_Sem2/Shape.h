#pragma once
#include <SFML\Graphics.hpp>
#include <memory>
using std::unique_ptr;

class Shape
{
public:
    Shape(sf::Color& color);
    virtual ~Shape()=0;

    // draw the shape to the window.
    virtual void draw(sf::RenderWindow& window)=0;

    //overridden by complex shapes
    virtual void setColor(sf::Color color);

    // Set the shape as chosen.
    //overridden by complex shapes
    virtual void setChosen(bool c);

    // set the shape to visible or invisible.
    void setVisible(bool v);

    bool getChosen() const;
    bool getVisible() const;

    sf::Color getColor() const;

    // return the are of the shape.
    virtual float getArea() const = 0;

    // return the diameter of the shape.
    virtual float getDiameter() const = 0;

protected:
    sf::Color _color;
    
    bool _chosen;

    bool _visible;
};

