#include "ShrinkCommand.h"
#include "TransformableShape.h"

const float ACTION_FACTOR = 0.8f;
const float UNDO_FACTOR = 1.25f;

ShrinkCommand::ShrinkCommand()
{
}


ShrinkCommand::~ShrinkCommand()
{}


void ShrinkCommand::action(Shape& shape)
{
    if (TransformableShape* s = dynamic_cast<TransformableShape*>(&shape))
        s->scale(ACTION_FACTOR);
}

void ShrinkCommand::undo(Shape& shape)
{
    if (TransformableShape* s = dynamic_cast<TransformableShape*>(&shape))
        s->scale(UNDO_FACTOR);
}
