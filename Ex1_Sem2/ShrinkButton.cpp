#include "ShrinkButton.h"
#include "Controller.h"
#include "TextureManager.h"

ShrinkButton::ShrinkButton(Controller& c, sf::Vector2f origin)
    :Button(c, origin)
{
    _shape.setTexture(&(TextureManager::getInstance().getMenuTexture(MenuTextures::Shrink)));
}


ShrinkButton::~ShrinkButton()
{}

void ShrinkButton::action()
{
    _controller.action(&_shrinkCommand, true);
}
