#include "Triangle.h"


Triangle::Triangle(sf::Color& color, sf::Vector2f origin)
    : TransformableShape(color, origin)
{
}


Triangle::~Triangle()
{}


void Triangle::updateCenter()
{
    float third = 1.f / 3.f;
    _center = { third*(_a.position.x + _b.position.x + _c.position.x), 
        third*(_a.position.y + _b.position.y + _c.position.y) };
}


void Triangle::draw(sf::RenderWindow& window)
{
    sf::ConvexShape c(3);
    c.setPoint(0, _a.position);
    c.setPoint(1, _b.position);
    c.setPoint(2, _c.position);

    c.setFillColor(_color);
    window.draw(c);
}

void Triangle::scale(float factor)
{

    sf::Transform t;
    t.scale({ factor, factor }, _center);
    _scaleFactor *= factor;

    _a = t.transformPoint(_a.position);
    _b = t.transformPoint(_b.position);
    _c = t.transformPoint(_c.position);

}

void Triangle::rotate(float rotate)
{
    sf::Transform t;
    t.rotate(rotate, _center);

    _a = t.transformPoint(_a.position);
    _b = t.transformPoint(_b.position);
    _c = t.transformPoint(_c.position);

    

}
