#pragma once
class Controller;

class ControllerCommand 
{
public:
    ControllerCommand(Controller& controller);
    virtual ~ControllerCommand() = 0;

protected:
    Controller& _controller;
};

