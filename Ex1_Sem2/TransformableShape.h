#pragma once
#include "Shape.h"
class TransformableShape :
    public Shape
{
public:
    TransformableShape(sf::Color& color, sf::Vector2f origin);
    virtual ~TransformableShape()=0;

    
    virtual void scale(float factor) = 0;
    virtual void rotate(float rotate) = 0;

protected:

    sf::Vector2f _origin;
    sf::Vector2f _center;
    float _scaleFactor;
    float _rotation;
};

