#pragma once
#include "TransformableShape.h"
#include <utility>
using std::pair;

class Square :
    public TransformableShape
{
public:

    Square(sf::Color& color, sf::Vector2f origin);
    ~Square();


    virtual void draw(sf::RenderWindow& window);

    virtual void scale(float factor);

    void setSize(float size);
    void rotate(float rotate);

    float getSize() const;

    virtual float getArea() const;
    virtual float getDiameter() const;

    sf::Vector2f getCenter() const;
    sf::Vector2f getTopLeft() const;

    void move10P(float pixels);

private:
    sf::Vertex _topLeft, _topRight, _bottomRight, _bottomLeft;
};

