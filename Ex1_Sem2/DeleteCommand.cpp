#include "DeleteCommand.h"
#include "Shape.h"

DeleteCommand::DeleteCommand()
{
}


DeleteCommand::~DeleteCommand()
{
}


void DeleteCommand::action(Shape& shape)
{
    shape.setVisible(false);
}

void DeleteCommand::undo(Shape& shape)
{
    shape.setVisible(true);
}
