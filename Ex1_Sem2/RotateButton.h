#pragma once
#include "Button.h"
#include "RotateCommand.h"
class Controller;


class RotateButton : public Button
{
public:
    RotateButton(Controller& controller, sf::Vector2f& origin);
    virtual ~RotateButton();

    void action();

private:
    RotateCommand _rotateCommand;
};

