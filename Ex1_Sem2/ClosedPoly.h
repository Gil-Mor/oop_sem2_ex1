#pragma once
#include "Polygon.h"
class ClosedPoly :
    public Polygon
{
public:

    ClosedPoly(sf::Color& color, sf::Vector2f origin);
    virtual ~ClosedPoly();

    void appendLastVertex(sf::Vector2f origin);

};

