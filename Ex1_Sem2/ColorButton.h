#pragma once
#include "Button.h"
#include "ColorCommand.h"

class ColorButton : public Button
{
public:
    ColorButton(Controller& controller, sf::Vector2f& origin);
    virtual ~ColorButton();

    void action();

    // over ride these functions so that this
    // button's color won't change.
    void setChosen(bool c);
    void setInFocus(bool f);

//private:
//    ColorCommand _colorCommand;
};

