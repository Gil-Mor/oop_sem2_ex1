#include "InfoWindow.h"
#include "windowProporties.h"
#include <string>


const string FONT_FILE = "../fonts/impact.ttf";

InfoWindow::InfoWindow()
    : _numOfSimpleShapes(0),
    _numOfComplexShapes(0),
    _numOfPolygons(0),
    _totalArea(0),
    _totalDiameter(0)
{
    _frame.setSize(INFO_WINDOW_SIZE);
    _frame.setPosition(INFO_WINDOW_ORIGIN);
    _frame.setFillColor(sf::Color::Red);

    _font.loadFromFile(FONT_FILE);
    _text.setFont(_font);
    _text.setCharacterSize(15);
    _text.setColor(sf::Color::White);

    _textOriginalPos = { _frame.getPosition().x + 5, _frame.getPosition().y + 5 };

}


InfoWindow::~InfoWindow()
{}

void InfoWindow::draw(sf::RenderWindow& window)
{
    _text.setPosition(_textOriginalPos);

    window.draw(_frame);

    float textX = _text.getPosition().x;
    float gapY = 18;

    _text.setString("Num Of Simple Shapes: " + std::to_string(_numOfSimpleShapes));

    window.draw(_text);

    _text.setPosition(textX, _text.getPosition().y + gapY);

    _text.setString("Num Of Complex Shapes: " + std::to_string(_numOfComplexShapes));

    window.draw(_text);

    _text.setPosition(textX, _text.getPosition().y + gapY);

    _text.setString("Num Of Polygons: " + std::to_string(_numOfPolygons));

    window.draw(_text);

    _text.setPosition(textX, _text.getPosition().y + gapY);

    _text.setString("Total Area: " + std::to_string(_totalArea));

    window.draw(_text);

    _text.setPosition(textX, _text.getPosition().y + gapY);

    _text.setString("Total Diameter: " + std::to_string(_totalDiameter));

    window.draw(_text);

    _text.setPosition(textX, _text.getPosition().y + gapY);
}

void InfoWindow::restart()
{
    _numOfSimpleShapes = 0;
    _numOfComplexShapes = 0;
    _numOfPolygons = 0;
}