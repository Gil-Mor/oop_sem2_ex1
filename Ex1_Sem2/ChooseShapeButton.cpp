#include "ChooseShapeButton.h"
#include "TextureManager.h"

ChooseShapeButton::ChooseShapeButton(Controller& controller,
    sf::Vector2f& origin, Controller::State state)
    : Button(controller, origin), _state(state)
{
    if (state == Controller::State::ChoosingForward_s)
        _shape.setTexture(&(TextureManager::getInstance().getMenuTexture(MenuTextures::ChooseForward)));
    else
        _shape.setTexture(&(TextureManager::getInstance().getMenuTexture(MenuTextures::ChooseBackward)));

}


ChooseShapeButton::~ChooseShapeButton()
{
}

void ChooseShapeButton::action()
{
    _controller.chooseShape(_state);

}
