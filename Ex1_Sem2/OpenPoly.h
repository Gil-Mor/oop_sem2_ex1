#pragma once
#include "Polygon.h"
class OpenPoly :
    public Polygon
{
public:
    OpenPoly(sf::Color& color, sf::Vector2f origin);
    virtual ~OpenPoly();

    void appendLastVertex(sf::Vector2f origin);

};

