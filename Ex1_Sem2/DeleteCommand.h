#pragma once
#include "TransformCommand.h"

class DeleteCommand :
    public TransformCommand
{
public:
    DeleteCommand();
    virtual ~DeleteCommand();


    void action(Shape& shape);
    void undo(Shape& shape);
};

