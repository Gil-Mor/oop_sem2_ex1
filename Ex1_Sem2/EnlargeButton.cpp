#include "EnlargeButton.h"
#include "Controller.h"
#include "TransformableShape.h"
#include "TextureManager.h"

const float ACTION_FACTOR = 1.25f;
const float UNDO_FACTOR = 0.8f;

EnlargeButton::EnlargeButton(Controller& c, sf::Vector2f origin)
    : Button(c, origin)
{
    _shape.setTexture(&(TextureManager::getInstance().getMenuTexture(MenuTextures::Enlarge)));
}


EnlargeButton::~EnlargeButton()
{
}

void EnlargeButton::action()
{  
    _controller.action(&_command, true);
}
