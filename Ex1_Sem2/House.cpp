#include "House.h"
#include <math.h>

const float EDGE_SIZE = 50;
const float ROOF_HEIGHT = (float)(EDGE_SIZE/2)*(float)sqrt(3);

House::House(sf::Color& color, sf::Vector2f origin)
    : TransformableShape(color, origin), 
    _squareSize(EDGE_SIZE), _roofHeight(ROOF_HEIGHT),
    _roof(color, origin), 
    _square(color, { origin.x - (EDGE_SIZE / 2), origin.y + ROOF_HEIGHT })
{
    _square.setSize(EDGE_SIZE);
    _roof.setSize(EDGE_SIZE);


    
}

House::~House()
{}

void House::draw(sf::RenderWindow& window)
{
    _square.setColor(_color);
    _roof.setColor(_color);
    _square.draw(window);
    _roof.draw(window);
}

void House::scale(float factor)
{
    _roofHeight *= factor;
    _squareSize *= factor;

    _center = _square.getCenter();

    _roof.setCenter(_center);

    _square.scale(factor);

    _roof.scale(factor);
}

void House::rotate(float rotate)
{

    _square.rotate(rotate);

    _center = _square.getCenter();
    _roof.setCenter(_center);

    _roof.rotate(rotate);
}

void House::move10P(float pixles)
{
    _square.move10P(pixles);
    _roof.move10P(pixles);
}

float House::getArea() const
{
    return _square.getArea() + _roof.getArea();

}

float House::getDiameter() const
{
    return _square.getDiameter() + _roof.getDiameter()
        - 2*_square.getSize();
}