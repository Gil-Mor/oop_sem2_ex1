#include "ColorButton.h"
#include "Controller.h"
#include "TextureManager.h"

ColorButton::ColorButton(Controller& controller, sf::Vector2f& origin)
    : Button(controller, origin)
{
    _shape.setTexture(&(TextureManager::getInstance().getMenuTexture(MenuTextures::Color)));
    _shape.setFillColor(_controller.getCurrColor());
}


ColorButton::~ColorButton()
{}

void ColorButton::action()
{
    _controller.changeColor();
    _shape.setFillColor(_controller.getCurrColor());
}

void ColorButton::setInFocus(bool f)
{
    _inFocus = f;
}

void ColorButton::setChosen(bool c)
{
    _chosen = c;
}
