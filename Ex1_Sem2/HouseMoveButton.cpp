#include "HouseMoveButton.h"
#include "Controller.h"
#include "TextureManager.h"

HouseMoveButton::HouseMoveButton(Controller& controller, sf::Vector2f& origin)
    : Button(controller, origin)
{
    _shape.setTexture(&(TextureManager::getInstance().getMenuTexture(MenuTextures::HouseMove)));
}


HouseMoveButton::~HouseMoveButton()
{
}

void HouseMoveButton::action()
{
    _controller.action(&_moveCommand, true);
}
