#include "EnlargeCommand.h"
#include "TransformableShape.h"

const float ACTION_FACTOR = 1.25f;
const float UNDO_FACTOR = 0.8f;

EnlargeCommand::EnlargeCommand()
{
}

EnlargeCommand::~EnlargeCommand()
{}

void EnlargeCommand::action(Shape& shape)
{
    if (TransformableShape* s = dynamic_cast<TransformableShape*>(&shape))
        s->scale(ACTION_FACTOR);
}

void EnlargeCommand::undo(Shape& shape)
{
    if (TransformableShape* s = dynamic_cast<TransformableShape*>(&shape))
        s->scale(UNDO_FACTOR);
}