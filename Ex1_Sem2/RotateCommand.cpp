#include "RotateCommand.h"
#include "TransformableShape.h"

const float ACTION_ROTATION = 90;
const float UNDO_ROTATION = -90;

RotateCommand::RotateCommand()
{
}


RotateCommand::~RotateCommand()
{
}

void RotateCommand::action(Shape& shape)
{
    if (TransformableShape* s = dynamic_cast<TransformableShape*>(&shape))
        s->rotate(ACTION_ROTATION);
}

void RotateCommand::undo(Shape& shape)
{
    if (TransformableShape* s = dynamic_cast<TransformableShape*>(&shape))
        s->rotate(UNDO_ROTATION);
}