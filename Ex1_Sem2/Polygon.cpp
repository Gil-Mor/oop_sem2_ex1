#include "Polygon.h"
#include <math.h>

Polygon::Polygon(sf::Color& color, sf::Vector2f origin)
    :StaticShape(color), _verticesCount(0), _diameter(0)
{
    appendVertex(origin);
}


Polygon::~Polygon()
{
}

void Polygon::appendVertex(sf::Vector2f origin)
{
    if (_verticesCount != 0)
    {
        _diameter += sqrt(pow(origin.x - _vertices[_verticesCount - 1].position.x, 2)
            + pow(origin.y - _vertices[_verticesCount - 1].position.y, 2));
    }
    _vertices.append(origin);
    _vertices[_verticesCount].color = _color;
    ++_verticesCount;
}

void Polygon::drawWhileDrawing(sf::RenderWindow& window, sf::Vector2f mouse)
{
    _vertices.append(mouse);
    _vertices[_verticesCount].color = _color;
    window.draw(_vertices);
    _vertices.resize(_verticesCount);
}

void Polygon::draw(sf::RenderWindow& window)
{

    for (unsigned i = 0; i < _vertices.getVertexCount(); ++i)
    {
        _vertices[i].color = _color;
    }

    window.draw(_vertices);
}

float Polygon::getArea() const
{
    return 0;
}

float Polygon::getDiameter() const
{
    return _diameter;
}
