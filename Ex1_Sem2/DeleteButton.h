#pragma once
#include "Button.h"
#include "DeleteCommand.h"

class DeleteButton :
    public Button
{
public:
    DeleteButton(Controller& controller, sf::Vector2f& origin);
    virtual ~DeleteButton();

    void action();

private:
    DeleteCommand _deleteCommand;
};

