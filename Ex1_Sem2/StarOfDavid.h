#pragma once
#include "TransformableShape.h"
#include "EquilateralTriangle.h"

class StarOfDavid : public TransformableShape
{
public:
    StarOfDavid(sf::Color& color, sf::Vector2f origin);
    ~StarOfDavid();

    void draw(sf::RenderWindow& window);
    void scale(float factor);
    void rotate(float rotate);

    virtual float getArea() const;
    virtual float getDiameter() const;

private:
    EquilateralTriangle _up, _down;
};

