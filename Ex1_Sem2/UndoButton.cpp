#include "UndoButton.h"
#include "Controller.h"
#include "TextureManager.h"

UndoButton::UndoButton(Controller& c, sf::Vector2f& origin)
    : Button(c, origin)
{
    _shape.setTexture(&(TextureManager::getInstance().getMenuTexture(MenuTextures::Undo)));
}


UndoButton::~UndoButton()
{}

void UndoButton::action()
{
    _controller.undo();

}



