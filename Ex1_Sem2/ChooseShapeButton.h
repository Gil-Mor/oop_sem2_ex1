#pragma once
#include "Button.h"
#include "Controller.h"

class ChooseShapeButton :
    public Button
{
public:
    ChooseShapeButton(Controller& controller,
        sf::Vector2f& origin, Controller::State state);

    virtual ~ChooseShapeButton();

    void action();

private:
    Controller::State _state;

};

