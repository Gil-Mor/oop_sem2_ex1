#pragma once
#include <SFML\Graphics.hpp>
#include <vector>
#include <memory>

using std::vector;
using std::unique_ptr;

namespace MenuTextures
{ 
    enum Tex
    {
        BG = 0,
        OpenPoly,
        ClosedPoly,
        Square,
        EquilateralTriangle,
        IsoscelesTriangle,
        House,
        HouseMove,
        StarOfDavid,

        New,
        Delete,
        Color,
        Rotate,
        Undo,
        Enlarge,
        Shrink,
        ChooseForward,
        ChooseBackward,
        SelectSimilars,


        NumOfButtons
    };
}

class TextureManager
{
public:

    static TextureManager& getInstance();


    virtual ~TextureManager();

    const sf::Texture& getMenuTexture(MenuTextures::Tex) const;

private:

    TextureManager();

    static unique_ptr<TextureManager> _instance;
    
    vector<sf::Texture> _menuTextures;

    void loadMenuTex();
};

