#pragma once
#include "TransformableShape.h"
class Triangle :
    public TransformableShape
{
public:
    Triangle(sf::Color& color, sf::Vector2f origin);
    virtual ~Triangle()=0;

    void updateCenter();

    void scale(float factor);
    void draw(sf::RenderWindow& window);
    void rotate(float rotate);

protected:
    sf::Vertex _a, _b, _c;
};

