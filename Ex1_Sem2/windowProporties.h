#pragma once
/* Defines size and originition of the main window, the menu, and the drawing board. */
#include <SFML\Graphics.hpp>

using std::string;

const string WINDOW_TITLE = "PAINTUSH";

const unsigned int FRAME_LIMIT = 50;

const sf::VideoMode FULL_SCREEN_MODE = sf::VideoMode::getDesktopMode();

const sf::Vector2u WINDOW_SIZE = { FULL_SCREEN_MODE.width - 20, FULL_SCREEN_MODE.height - 100 };
const sf::Vector2i WINDOW_ORIGIN = { 0, 0 };

const sf::Vector2f MENU_SIZE = { 200, float(FULL_SCREEN_MODE.height) };
const sf::Vector2f MENU_ORIGIN = { float(FULL_SCREEN_MODE.width - MENU_SIZE.x), 0 };

const sf::Vector2f INFO_WINDOW_SIZE = { MENU_ORIGIN.x, 100};
const sf::Vector2f INFO_WINDOW_ORIGIN = { 0, 0 };


const sf::Vector2f BOARD_SIZE = { float(WINDOW_SIZE.x - MENU_SIZE.x), (float)WINDOW_SIZE.y };
const sf::Vector2f BOARD_ORIGIN = { float(WINDOW_ORIGIN.x), float(WINDOW_ORIGIN.y) };

