#pragma once

/* Controller of the painter program.*/
#include <vector>
#include <memory>
#include "Menu.h"
#include "InfoWindow.h"

using std::vector;
using std::unique_ptr;

class Shape;
class Polygon;
class TransformableShape;
class TransformCommand;

class Controller
{
public:

    // The currnet state. Mostly the next shape to draw..
    // The Buttons set the state.
    enum State
    {
        Nothing_s = 0,
        DrawingOpenPoly_s,
        DrawingClosedPoly_s,
        DrawingSquare_s,
        DrawingHouse_s,
        DrawingEqTriangle_s,
        DrawingIsoTriangle_s,
        DrawingStarOfDavid_s,
        ChoosingBackward_s,
        ChoosingForward_s,

        Exiting_s,
    };

    Controller();
    ~Controller();

    /* Runs a loop that channels control to differnt
    parts according to the current state. */
    void run();

    /* Set the currnet state of the program.
    This is used by defferent parts of the program
    through a StateCommand class which encapsulates
    a call to this function. */
    void setState(State state);
    Controller::State getState() const;

    void selectSimilars();

    /* Gets a "TransformCommand" from the Buttons and performs
    it on the currently chosen shapes.
    Sets this command to be the lastTransformation that
    will be used in undo..*/
    void action(TransformCommand* command, bool way);

    /* undo the last transformatoin.*/
    void undo();

    /* if the last trasnformatin was a drawing then
    remove the last shape from the shape array.*/
    void undoDrawing();

    /* Tell if there are temporary deleted shpaes.
    (shapes that are deleted but can still be undeleted with undo
    until the user performs the next operation.*/
    void setTmpDeleted(bool tmoDeleted);

    /* delete the temporary deleted shapes if there are any.
    shrink the shapes array.*/
    void finalDeleteChosenShapes();

    /* Start a new draw page.*/
    void restart();

    /* change the color for future shapes.*/
    void changeColor();

    /*used by Color Button to synd his color with the currnet color. */
    sf::Color getCurrColor() const;

    void chooseShape(Controller::State state);

private:

    //=============== SHAPES =============
    vector<unique_ptr<Shape>> _shapes;
    void addShape(unique_ptr<Shape>);

    //========== INFO WINDOW ===========
    /* little bar with information about the shapes on the board.*/
    InfoWindow _infoWindow;

    // update the infowindow before each draw
    void updateInfoWindow();
    void countShapes();
    void countArea();
    void countDiameter();


    // undo dtruct holds information about the undo state.
    struct Undo
    {
        // true if the last operation was a shape drawing.
        bool drawnSomthing;

        // true if the next undo is an undo operation.
        // false if the next undo is an "undo the undo" (redo) operation.
        bool undoTransform;

        // for keeping the last removed shape when undoing a drawing.
        // it's different from delete because this shape
        // isn't chosen.
        unique_ptr<Shape> removedShape;

        // signal that there are temporary deleted shpaes.
        bool tmpDeleted;

    } _undo;   
    void InitUndo();

    // the index of the current color in the colors array.
    unsigned _currColor;

    void InitColors();


    // struct to save information about the chosen shapes.
    struct ChoosenShape
    {
        // true if there's a single chosen shape.
        bool singleChosen;

        // true if the user chosed similar shapes with the chose similar button
        bool similarsChosen;
        
        // index for the arrow keys choice. not connected to the similarsArray.
        unsigned index;     
     } _chosenShape;



    // chose shape when the user presses aroow keys.
    void handleKeyBoard(sf::Event& event);

    void InitChosenShapes();
    void clearChosenShapes();
    void clearChosenSimilars();
    

    // keeps the last Transforamtion Command that works on chosen Shapes.
    TransformCommand* _lastTransform;


    //=============== STATE =============
    /* current state of the program. */
    State _state;
    void InitState();


    // ================= MENU ==================
    /* the main menu. */
    Menu _menu;


    /* return true if the mouse is over the board area. */
    bool mouseOverBoard(const sf::Vector2f mouse) const;

    // draw shapes on the board if the user draws somthing.
    void runBoard(sf::Event& event, sf::Vector2f mouse);

    // different loop when the user is drawing a polygon.
    void drawPoly(sf::Vector2f mouse, Polygon* poly);

    //=================== WINDOW ================
    /* SFML window. */
    sf::RenderWindow _mainWindow;
    void InitWindow();
    void draw();
    void drawShapes();

    // handle window resizing and closing.
    void handleGeneralWindowEvents(sf::Event event);

};

