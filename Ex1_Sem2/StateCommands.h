#pragma once

/* Inherits from Command.
Encapsulates calls to get and set state member functions
of the controller to get and set the global state of
the program. */

#include "ControllerCommand.h"
#include "Controller.h"

class StateCommands : public ControllerCommand
{
public:

    StateCommands(Controller& controller);
    virtual ~StateCommands();

    /* "Inject" the wanted state into this class before calling
    action() to change the state in the controller. */
    void setState(Controller::State state);

    /* Get the current state from the controller 
    (not the currrent state that is defined at the moment in this class). */
    Controller::State getState() const;
//private:
//    Controller::State _state;
};

