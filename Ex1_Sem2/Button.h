#pragma once
#include <memory>
#include <SFML\Graphics.hpp>

class Controller;

const sf::Vector2f BUTTON_SIZE = { 50, 50 };


const sf::Color BUTTON_FOCUS_COLOR = { 150, 0, 0, 200 };
const sf::Color BUTTON_NO_FOCUS_COLOR = sf::Color::White;

const sf::Color BUTTON_PRESSED_COLOR = { 200, 32, 180, 250 };

const sf::Color BUTTON_BG_NO_FOCUS_COLOR = { 255, 100, 255, 255 };
const sf::Color BUTTON_BG_FOCUS_COLOR = { 50, 232, 240, 250 };

class Button
{
public:

    Button(Controller& controller, sf::Vector2f& origin);

    virtual ~Button()=0;

    virtual void action()=0;
    /* Returns true if the mouse is over this object RectangleShape.
    the mouse coordinatesare given in 'world' coordinates -
    from window.mapPixelToCoords function.
    That's why Vector2f and not 2i. */
    virtual bool mouseOver(const sf::Vector2f& mouse) const;

    /* Set the object i and out of focus. 
    ColorButton overrides this.*/
    virtual void setInFocus(bool f);

    /* Set the object chosen flag.
    ColorButton overrides this.*/
    virtual void setChosen(bool c);

    /* returns true if this object is currently in focus.*/
    bool getInFocus() const;

    /* returns true if this object is currently chosen.*/
    bool getChosen() const;

    void draw(sf::RenderWindow& window) const;

protected:

    Controller& _controller;
    sf::RectangleShape _shape;

    bool _inFocus;
    bool _chosen;


};



