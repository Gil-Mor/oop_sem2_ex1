#include "Square.h"

const float SIZE = 30;

Square::Square(sf::Color& color, sf::Vector2f origin)
    :TransformableShape(color, origin)
{
    _topLeft = origin;
    _topRight = sf::Vector2f(origin.x + SIZE, origin.y);
    _bottomRight = sf::Vector2f(origin.x + SIZE, origin.y + SIZE);
    _bottomLeft = sf::Vector2f(origin.x, origin.y + SIZE);
}


Square::~Square()
{
}

sf::Vector2f Square::getCenter() const
{
    return{ _topLeft.position.x + (_topRight.position.x - _topLeft.position.x) / 2,
        _topLeft.position.y + (_bottomLeft.position.y - _topLeft.position.y) / 2};
}

sf::Vector2f Square::getTopLeft() const
{
    return _topLeft.position;
}

void Square::draw(sf::RenderWindow& window)
{
    sf::ConvexShape s(4);
    s.setPoint(0, _topLeft.position);
    s.setPoint(1, _topRight.position);
    s.setPoint(2, _bottomRight.position);
    s.setPoint(3, _bottomLeft.position);
    s.setFillColor(_color);

    window.draw(s);

}

void Square::scale(float factor)
{
    sf::Transform t;

    t.scale({ factor, factor }, getCenter());

    _topLeft = t.transformPoint(_topLeft.position);
    _topRight = t.transformPoint(_topRight.position);
    _bottomRight = t.transformPoint(_bottomRight.position);
    _bottomLeft = t.transformPoint(_bottomLeft.position);

}

void Square::setSize(float size)
{
    sf::Transform t;

    t.scale({ size / getSize(), size / getSize() }, _topLeft.position);

    _topLeft = t.transformPoint(_topLeft.position);
    _topRight = t.transformPoint(_topRight.position);
    _bottomRight = t.transformPoint(_bottomRight.position);
    _bottomLeft = t.transformPoint(_bottomLeft.position);
}

void Square::move10P(float pixles)
{
    sf::Transform t;
    t.translate(0, pixles);

    _topLeft = t.transformPoint(_topLeft.position);
    _topRight = t.transformPoint(_topRight.position);
    _bottomRight = t.transformPoint(_bottomRight.position);
    _bottomLeft = t.transformPoint(_bottomLeft.position);

}

void Square::rotate(float rotate)
{
}

float Square::getSize() const
{
    return _topRight.position.x - _topLeft.position.x;
        
}

float Square::getArea() const
{
    return pow(getSize(), 2);
}

float Square::getDiameter() const
{
    return getSize() * 4;
}