#pragma once
#include <SFML\Graphics.hpp>

class InfoWindow
{
public:
    InfoWindow();
    virtual ~InfoWindow();

    int _numOfSimpleShapes,
        _numOfComplexShapes,
        _numOfPolygons;

    float _totalArea,
        _totalDiameter;

    void draw(sf::RenderWindow& window);

    void restart();

private:
    sf::RectangleShape _frame;
    sf::Font _font;

    sf::Vector2f _textOriginalPos;
    sf::Text _text;
};

