#pragma once
#include "Button.h"
class NewButton :
    public Button
{
public:
    NewButton(Controller& c, sf::Vector2f origin);
    virtual ~NewButton();

    void action();
};

