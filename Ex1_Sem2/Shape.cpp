#include "Shape.h"

const int INCREASE_CHOSEN_ALPHA = 100;

Shape::Shape(sf::Color& color)
    :_color(color), _chosen(false), _visible(true)
{ 
}


Shape::~Shape()
{
}

void Shape::setColor(sf::Color color)
{
    _color = color;
}

void Shape::setVisible(bool v)
{
    _visible = v;
}

bool Shape::getVisible() const
{
    return _visible;
}

void Shape::setChosen(bool c)
{
    if (c && _chosen || !c && !_chosen)
        return;

    _chosen = c;
    if (c)
    {
        _color.a += INCREASE_CHOSEN_ALPHA;
    }
    else
        _color.a -= INCREASE_CHOSEN_ALPHA;
    
}

bool Shape::getChosen() const
{
    return _chosen;
}

sf::Color Shape::getColor() const
{
    return _color;
}




