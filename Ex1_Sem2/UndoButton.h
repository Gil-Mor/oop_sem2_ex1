#pragma once
#include "Button.h"

class UndoButton : public Button
{
public:
    UndoButton(Controller& c, sf::Vector2f& origin);
    ~UndoButton();

    void action();
};

