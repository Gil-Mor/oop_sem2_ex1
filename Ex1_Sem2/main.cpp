#include "Controller.h"

int main()
{
    // Initialize the Controller.
    Controller c;

    // run the program.
    c.run();

    return 0;
}