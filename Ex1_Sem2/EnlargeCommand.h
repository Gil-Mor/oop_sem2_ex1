#pragma once
#include "TransformCommand.h"

class EnlargeCommand : public TransformCommand
{
public:
    EnlargeCommand();
    virtual ~EnlargeCommand();


    void action(Shape& shape);
    void undo(Shape& shape);
};

