#include "DeleteButton.h"
#include "Controller.h"
#include "TextureManager.h"

DeleteButton::DeleteButton(Controller& controller, sf::Vector2f& origin)
    : Button(controller, origin)
{
    _shape.setTexture(&(TextureManager::getInstance().getMenuTexture(MenuTextures::Delete)));
}


DeleteButton::~DeleteButton()
{}

void DeleteButton::action()
{
    _controller.action(&_deleteCommand, true);
}
