#pragma once
#include "TransformableShape.h"
#include "Square.h"
#include "EquilateralTriangle.h"

class House : public TransformableShape
{
public:
   
    House(sf::Color& color, sf::Vector2f origin);
    ~House();

    void draw(sf::RenderWindow& window);
    void scale(float factor);
    void rotate(float rotate);

    virtual float getArea() const;
    virtual float getDiameter() const;

    void move10P(float pixles);

private:
    Square _square;
    EquilateralTriangle _roof;
    sf::VertexArray _vertices;
    float _squareSize, _roofHeight;
};

