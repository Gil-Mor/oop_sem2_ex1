#pragma once
#include "Button.h"
#include "EnlargeCommand.h"
class Controller;

class EnlargeButton :
    public Button
{
public:
    EnlargeButton(Controller& c, sf::Vector2f origin);
    virtual ~EnlargeButton();

    void action();

private:
    EnlargeCommand _command;
};

