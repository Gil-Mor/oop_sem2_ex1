#pragma once
#include "Button.h"

class Controller;

class SelectSimilarButton :
    public Button
{
public:
    SelectSimilarButton(Controller& c, sf::Vector2f origin);
    virtual ~SelectSimilarButton();

    virtual void action();
};

