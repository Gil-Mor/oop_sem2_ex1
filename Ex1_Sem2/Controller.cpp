#include "Controller.h"
#include "windowProporties.h"
#include "Polygon.h"
#include "OpenPoly.h"
#include "ClosedPoly.h"
#include "Square.h"
#include "House.h"
#include "TransformableShape.h"
#include "TransformCommand.h"
#include "EquilateralTriangle.h"
#include "IsoscelesTriangle.h"
#include "StarOfDavid.h"
#include <typeinfo>

// This needs to be global so it will be initialized first
// so that the color button could use this vector.
vector<sf::Color> SHAPES_COLORS = { { 255, 0, 0, 150 }, { 0, 255, 0, 150 }, { 0, 0, 255, 150 } };

Controller::Controller()
    : _currColor(0),
    _menu(*this)
{
    InitWindow();
    InitUndo();
    InitState();
    InitChosenShapes();
}

void Controller::InitUndo()
{
    _lastTransform = nullptr;
    _undo.undoTransform = false;
    _undo.drawnSomthing = false;
    _undo.tmpDeleted = false;
}

void Controller::InitChosenShapes()
{
    _chosenShape.singleChosen = false;
    _chosenShape.similarsChosen = false;
    _chosenShape.index = 0;

    _infoWindow.restart();
}

void Controller::InitState()
{
    _state = DrawingSquare_s;
}

void Controller::InitColors()
{
    _currColor = 0;
}

Controller::~Controller()
{}

void Controller::restart()
{

    InitUndo();
    InitState();
    InitColors();
    InitChosenShapes();

    _shapes.clear();

    run();

}

void Controller::InitWindow()
{
    _mainWindow.create(FULL_SCREEN_MODE, WINDOW_TITLE);

    _mainWindow.setSize(WINDOW_SIZE);

    // set window position to the top left corner of the screen
    _mainWindow.setPosition(WINDOW_ORIGIN);

    // set some frame limit.. we don't need much..
    _mainWindow.setFramerateLimit(FRAME_LIMIT);
}

void Controller::run()
{

    sf::Vector2f mouse;
    sf::Event event;

    // play while we're not exisiting.
    while (getState() != Controller::State::Exiting_s)
    {

        while (_mainWindow.pollEvent(event))
        {
            // handle stufflike resizing, closing, etc'...
            handleGeneralWindowEvents(event);

            // check if arraow key was pressed and if so chose shape
            handleKeyBoard(event);

            // get"real" coords of the mouse.
            mouse = _mainWindow.mapPixelToCoords(sf::Mouse::getPosition(_mainWindow));

            // if the mouse is over the menu, run the menu.
            if (_menu.mouseOver(mouse))
            {
                _menu.handleEvent(event, mouse);
            }

            // else run the board. 
            // here we handle drawing new shapes when the mouse clicks
            // over the board.
            else if (mouseOverBoard(mouse))
            {
                runBoard(event, mouse);
            }


        }

        _mainWindow.clear();
        draw();
        _mainWindow.display();

    }

    _mainWindow.close();

}

void Controller::handleKeyBoard(sf::Event& event)
{

    if (event.type == sf::Event::KeyPressed)
    {
        if (event.key.code == sf::Keyboard::Right)
        {
            chooseShape(Controller::State::ChoosingForward_s);
        }
        else if (event.key.code == sf::Keyboard::Left)
        {
            chooseShape(Controller::State::ChoosingBackward_s);
        }
    }
}

void Controller::chooseShape(Controller::State state)
{

    clearChosenSimilars();

    bool look = true;

    switch (state)
    {
        case Controller::State::ChoosingForward_s:
        {

            if (!_chosenShape.singleChosen)
            {
                _chosenShape.index = 0;
            }

            else if (_chosenShape.index >= _shapes.size() - 1)
            {
                look = false;
                _chosenShape.singleChosen = false;
            }

            else
                ++_chosenShape.index;
            
            break;
        }

        case Controller::State::ChoosingBackward_s:
        {
            if (!_chosenShape.singleChosen)
            {
                _chosenShape.index = _shapes.size() - 1;
            }

            else if (_chosenShape.index <= 0 )
            {
                look = false;
                _chosenShape.singleChosen = false;
            }

            else
                --_chosenShape.index;

            break;
        }

        default:
            break;
    }

    if (look)
    {
        _chosenShape.singleChosen = false;
        while (0 <= _chosenShape.index && _chosenShape.index < _shapes.size())
        {

            if (_shapes[_chosenShape.index]->getVisible())
            {
                _chosenShape.singleChosen = true;
                break;
            }
            if (state == State::ChoosingForward_s)
                ++_chosenShape.index;
            else
                --_chosenShape.index;
        }

    }

    if (_chosenShape.singleChosen)
    {
        _shapes[_chosenShape.index]->setChosen(true);
    }

}

void Controller::selectSimilars()
{

    if (_chosenShape.similarsChosen)
    {
        _chosenShape.similarsChosen = false;
        clearChosenSimilars();
        _shapes[_chosenShape.index]->setChosen(true);
        return;
    }

    if (!_chosenShape.singleChosen)
    {
        _chosenShape.similarsChosen = false;
        return;
    }

    for (unsigned i = 0; i < _shapes.size(); ++i)
    {

        if ((typeid(*_shapes[i])) == typeid(*_shapes[_chosenShape.index]))
        {
            _shapes[i]->setChosen(true);
        }
        else
            _shapes[i]->setChosen(false);
    }

    _chosenShape.similarsChosen = true;
}

void Controller::clearChosenShapes()
{
    _chosenShape.singleChosen = false;
    clearChosenSimilars();
}

void Controller::clearChosenSimilars()
{
    _chosenShape.similarsChosen = false;

    for (unsigned i = 0; i < _shapes.size(); ++i)
    {
        if (_shapes[i]->getChosen())
            _shapes[i]->setChosen(false);
    }

}

void Controller::runBoard(sf::Event& event, sf::Vector2f mouse)
{
    if (event.type == sf::Event::MouseButtonPressed
        && event.mouseButton.button == sf::Mouse::Left)
    {
        // operate according to state.
        switch (getState())
        {
            case Controller::State::Nothing_s:
                break;

            case Controller::DrawingClosedPoly_s:
            {
                drawPoly(mouse, new ClosedPoly(SHAPES_COLORS[_currColor], mouse));
                setState(DrawingClosedPoly_s);
                break;
            }
            case Controller::DrawingOpenPoly_s:
            {
                drawPoly(mouse, new OpenPoly(SHAPES_COLORS[_currColor], mouse));
                setState(DrawingOpenPoly_s);
                break;
            }
            case Controller::DrawingSquare_s:
            {
                addShape(unique_ptr<Shape>(new Square(SHAPES_COLORS[_currColor], mouse)));
                break;
            }

            case DrawingEqTriangle_s:
            {
                addShape(unique_ptr<Shape>(new EquilateralTriangle(SHAPES_COLORS[_currColor], mouse)));
                break;
            }

            case DrawingIsoTriangle_s:
            {
                addShape(unique_ptr<Shape>(new IsoscelesTriangle(SHAPES_COLORS[_currColor], mouse)));
                break;
            }
            case DrawingStarOfDavid_s:
            {
                addShape(unique_ptr<Shape>(new StarOfDavid(SHAPES_COLORS[_currColor], mouse)));
                break;
            }
            case DrawingHouse_s:
            {
                addShape(unique_ptr<Shape>(new House(SHAPES_COLORS[_currColor], mouse)));
                break;
            }

            case Controller::Exiting_s:
                break;

            default:
                break;
        }
    }
}

void Controller::action(TransformCommand* command, bool action)
{
    if (action && _undo.tmpDeleted)
    {
        finalDeleteChosenShapes();
        _undo.tmpDeleted = false;
    }
    _lastTransform = command;

    if (action) {
        _undo.undoTransform = true;
        _undo.drawnSomthing = false;
    }
    else
        _undo.undoTransform = false;

    if (_chosenShape.similarsChosen)
    {
        for (unsigned i = 0; i < _shapes.size(); ++i)
        {
            if (_shapes[i]->getChosen())
            {
                if (action)
                    command->action(*_shapes[i]);
                else
                    command->undo(*_shapes[i]);
            }
        }
    }

    else if (_chosenShape.singleChosen)
    {
        if (action)
            command->action(*_shapes[_chosenShape.index]);
        else
            command->undo(*_shapes[_chosenShape.index]);
    }

    else
        undoDrawing();
}

void Controller::undo()
{
    if (_lastTransform)
    {
        if (_undo.undoTransform)
            action(_lastTransform, false);
        else
            action(_lastTransform, true);
    }
    else
    {
        undoDrawing();
    }
}


void Controller::undoDrawing()
{
    if (_undo.drawnSomthing && !_shapes.empty())
    {
        _undo.removedShape.reset((_shapes[_shapes.size() - 1].release()));
        _shapes.pop_back();
        _undo.drawnSomthing = false;
    }
    else if (_undo.removedShape)
    {
        _shapes.emplace_back(_undo.removedShape.release());
        _undo.removedShape.reset(nullptr);
        _undo.drawnSomthing = true;
    }
}

void Controller::setTmpDeleted(bool tmpDeleted)
{
    _undo.tmpDeleted = tmpDeleted;
}

void Controller::finalDeleteChosenShapes()
{
    if (_chosenShape.similarsChosen)
    {
        for (unsigned i = 0; i < _shapes.size(); ++i)
        {
            if (!_shapes[i]->getVisible())
            {
                // update number of shapes in info..
                if (dynamic_cast<Polygon*>(_shapes[i].get()))
                    _infoWindow._numOfPolygons--;

                else if (dynamic_cast<StarOfDavid*>(_shapes[i].get())
                    || dynamic_cast<House*>(_shapes[i].get()))
                    _infoWindow._numOfComplexShapes--;

                else
                    _infoWindow._numOfSimpleShapes--;


                _shapes[i].release();
                _shapes[i] = nullptr;
            }

        }
        _shapes.shrink_to_fit();
        _chosenShape.similarsChosen = false;

    }
    else if (_chosenShape.singleChosen)
    {
        if (0 <= _chosenShape.index && _chosenShape.index < _shapes.size())
        {
            _shapes[_chosenShape.index].release();
            _shapes[_chosenShape.index] = nullptr;
            _shapes.shrink_to_fit();
            _chosenShape.singleChosen = false;
        }
        _shapes.shrink_to_fit();
        _chosenShape.singleChosen = false;
    }


}


void Controller::handleGeneralWindowEvents(sf::Event event)
{
    switch (event.type)
    {
        // catch the resize events
        case sf::Event::Resized:
        {
            // update the view to the new size of the window
            sf::FloatRect visible(0.f, 0.f, (float)event.size.width, (float)event.size.height);
            _mainWindow.setView(sf::View(visible));
            break;
        }

        case sf::Event::Closed:
            setState(Controller::State::Exiting_s);
            break;

        case sf::Event::KeyPressed:
        {
            if (event.key.code == sf::Keyboard::Escape)
            {
                setState(Controller::State::Exiting_s);
            }

            break;
        }
    }
}

void Controller::drawPoly(sf::Vector2f mouse, Polygon* poly)
{
    sf::Event event;

    while (getState() != Controller::State::Nothing_s)
    {
        while (_mainWindow.pollEvent(event))
        {
            mouse = _mainWindow.mapPixelToCoords(sf::Mouse::getPosition(_mainWindow));

            switch (event.type)
            {
                case sf::Event::MouseButtonPressed:
                {
                    if (event.mouseButton.button == sf::Mouse::Left)
                    {
                        poly->appendVertex(mouse);
                    }

                    // right click draws the last vertex.
                    if (event.mouseButton.button == sf::Mouse::Right)
                    {
                        poly->appendLastVertex(mouse);
                        setState(Controller::State::Nothing_s);
                    }

                }

                default:
                    break;
            }

        }
        _mainWindow.clear();
        draw();
        poly->drawWhileDrawing(_mainWindow, mouse);
        _mainWindow.display();

    }
    addShape(unique_ptr<Shape>(poly));
}

void Controller::addShape(unique_ptr<Shape> shape)
{
    // has to check this beforelosing the pointer to te vector
    if (dynamic_cast<Polygon*>(shape.get()))
        _infoWindow._numOfPolygons++;

    else if (dynamic_cast<StarOfDavid*>(shape.get())
        || dynamic_cast<House*>(shape.get()))
    {
        _infoWindow._numOfComplexShapes++;
    }

    else
        _infoWindow._numOfSimpleShapes++;

    _shapes.push_back(std::move(shape));
    _undo.drawnSomthing = true;
    _lastTransform = nullptr;
    clearChosenShapes();
}

void Controller::draw()
{
    drawShapes();
    _menu.draw(_mainWindow);
    updateInfoWindow();
    _infoWindow.draw(_mainWindow);

}

void Controller::updateInfoWindow()
{
    countShapes();
    countArea();
    countDiameter();
}

void Controller::countShapes()
{
    _infoWindow._numOfPolygons = 0;
    _infoWindow._numOfSimpleShapes = 0;
    _infoWindow._numOfComplexShapes = 0;

    for (unsigned i = 0; i < _shapes.size(); ++i)
    {
        if (_shapes[i]->getVisible())
        {
            if (dynamic_cast<Polygon*>(_shapes[i].get()))
                ++_infoWindow._numOfPolygons;

            else if (dynamic_cast<StarOfDavid*>(_shapes[i].get())
                || dynamic_cast<House*>(_shapes[i].get()))
                ++_infoWindow._numOfComplexShapes;

            else
                ++_infoWindow._numOfSimpleShapes;
        }
    }
}

void Controller::countArea()
{
    _infoWindow._totalArea = 0;
    for (unsigned i = 0; i < _shapes.size(); ++i)
    {
        if (_shapes[i]->getVisible())
            _infoWindow._totalArea += _shapes[i]->getArea();
    }
}

void Controller::countDiameter()
{
    _infoWindow._totalDiameter = 0;
    for (unsigned i = 0; i < _shapes.size(); ++i)
    {
        if (_shapes[i]->getVisible())
            _infoWindow._totalDiameter += _shapes[i]->getDiameter();
    }
}

void Controller::drawShapes()
{
    for (unsigned int i = 0; i < _shapes.size(); ++i)
    {
        if (_shapes[i]->getVisible())
            _shapes[i]->draw(_mainWindow);
    }
}

void Controller::changeColor()
{

    _currColor = ((_currColor + 1) % SHAPES_COLORS.size());


}

sf::Color Controller::getCurrColor() const
{
    return SHAPES_COLORS[_currColor];
}

void Controller::setState(Controller::State state)
{
    _state = state;
}

Controller::State Controller::getState() const
{
    return _state;
}

bool Controller::mouseOverBoard(const sf::Vector2f mouse) const
{
    float startX = BOARD_ORIGIN.x;

    float startY = BOARD_ORIGIN.y;

    float endX = (startX + BOARD_SIZE.x);

    float endY = (startY + BOARD_SIZE.y);

    return startX < mouse.x && mouse.x < endX
        && startY < mouse.y && mouse.y < endY;
}

