#pragma once
#include "TransformCommand.h"
class Shape;

class HouseMoveCommand :
    public TransformCommand
{
public:
    HouseMoveCommand();
    virtual ~HouseMoveCommand();

    void action(Shape& shape);
    void undo(Shape& shape);
};

