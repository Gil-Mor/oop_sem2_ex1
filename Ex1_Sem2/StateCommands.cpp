#include "StateCommands.h"

StateCommands::StateCommands(Controller& controller)
    :ControllerCommand(controller)/*, _state(Controller::State::DrawingSquare_s)*/
{
}

StateCommands::~StateCommands()
{}

void StateCommands::setState(Controller::State state)
{
    _controller.setState(state);
}

Controller::State StateCommands::getState() const
{
    return _controller.getState();
}
