#include "OpenPoly.h"


OpenPoly::OpenPoly(sf::Color& color, sf::Vector2f origin)
    : Polygon(color, origin)
{
    _vertices.setPrimitiveType(sf::PrimitiveType::LinesStrip);
}


OpenPoly::~OpenPoly()
{
}

void OpenPoly::appendLastVertex(sf::Vector2f origin)
{
    if (_verticesCount != 0)
    {
        _diameter += sqrt(pow(origin.x - _vertices[_verticesCount - 1].position.x, 2)
            + pow(origin.y - _vertices[_verticesCount - 1].position.y, 2));
    }
    _vertices.append(origin);
    _vertices[_verticesCount].color = _color;
    ++_verticesCount;
}


