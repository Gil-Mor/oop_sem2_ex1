#include "EquilateralTriangle.h"
#include <cmath>
#include <utility>      // std::pair

const float EDGE_SIZE = 30;
const float HEIGHT = (float)(EDGE_SIZE/2)*sqrt(3);


EquilateralTriangle::EquilateralTriangle(sf::Color& color, sf::Vector2f origin)
    : Triangle(color, origin), _size(EDGE_SIZE), _height(HEIGHT)
{
    _a = origin;  
    _b = sf::Vector2f(origin.x - _size / 2, origin.y + _height);
    _c = sf::Vector2f(origin.x + _size / 2, origin.y + _height);

    float third = 1.f / 3.f;
    _center = { third*(_a.position.x + _b.position.x + _c.position.x),
        third*(_a.position.y + _b.position.y + _c.position.y) };

}

EquilateralTriangle::~EquilateralTriangle()
{}

void EquilateralTriangle::setCenter(sf::Vector2f center)
{
    _center = center;
}

void EquilateralTriangle::setSize(float size)
{
    sf::Transform t;

    t.scale({ size / _size, size / _size }, _a.position);

    _a = t.transformPoint(_a.position);
    _b = t.transformPoint(_b.position);
    _c = t.transformPoint(_c.position);

}

void EquilateralTriangle::move10P(float pixles)
{
    sf::Transform t;
    t.translate(0, pixles);

    _a = t.transformPoint(_a.position);
    _b = t.transformPoint(_b.position);
    _c = t.transformPoint(_c.position);
}

float EquilateralTriangle::getArea() const
{
    float size = _size*_scaleFactor;
    return (size)*((size / 2) * (float)sqrt(3))/2;
}

float EquilateralTriangle::getDiameter() const
{
    return getEdgeSize() * 3;
}

float EquilateralTriangle::getEdgeSize() const
{
    return _size*_scaleFactor;
}

sf::Vector2f EquilateralTriangle::getAVertex() const
{
    return _a.position;
}
