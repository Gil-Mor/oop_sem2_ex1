#pragma once
#include "Button.h"
#include "Controller.h"

class DrawingButton :
    public Button
{
public:
    DrawingButton(Controller& c, sf::Vector2f& origin,
        const sf::Texture*, Controller::State);
    virtual ~DrawingButton();

    void action();

protected:
    Controller::State _shapeState;
};

