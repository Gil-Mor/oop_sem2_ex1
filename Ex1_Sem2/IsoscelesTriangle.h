#pragma once
#include "Triangle.h"
class IsoscelesTriangle : public Triangle
{
public:
    IsoscelesTriangle(sf::Color& color, sf::Vector2f origin);
    virtual ~IsoscelesTriangle();

    virtual float getArea() const;
    virtual float getDiameter() const;
};

