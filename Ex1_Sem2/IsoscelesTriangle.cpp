#include "IsoscelesTriangle.h"

const float BASE_SIZE = 35;
const float SIDE_SIZE = 50;
const float HEIGHT = (float)sqrt(pow(SIDE_SIZE, 2) - pow((BASE_SIZE / 2), 2));

IsoscelesTriangle::IsoscelesTriangle(sf::Color& color, sf::Vector2f origin)
    : Triangle(color, origin)
{
    _a = origin;
    _b = sf::Vector2f(_a.position.x - BASE_SIZE / 2, _a.position.y + HEIGHT);
    _c = sf::Vector2f(_a.position.x + BASE_SIZE / 2, _a.position.y + HEIGHT);

    updateCenter();
}


IsoscelesTriangle::~IsoscelesTriangle()
{}

float IsoscelesTriangle::getArea() const
{
    return (BASE_SIZE*_scaleFactor*HEIGHT*_scaleFactor) / 2;
}


float IsoscelesTriangle::getDiameter() const
{
    return (BASE_SIZE + 2 * SIDE_SIZE)*_scaleFactor;
}
