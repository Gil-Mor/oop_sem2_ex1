#pragma once
#include "Shape.h"
class StaticShape :
    public Shape
{
public:
    StaticShape(sf::Color& color);
    virtual ~StaticShape();
};

