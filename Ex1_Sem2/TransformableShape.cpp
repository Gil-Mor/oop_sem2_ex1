#include "TransformableShape.h"


TransformableShape::TransformableShape(
    sf::Color& color, sf::Vector2f origin)
    :
    Shape(color), _origin(origin), _scaleFactor(1),
    _rotation(0)
{
}


TransformableShape::~TransformableShape()
{
}
