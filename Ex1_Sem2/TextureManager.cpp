#include "TextureManager.h"

unique_ptr<TextureManager> TextureManager::_instance = nullptr;

const std::string MENU_TEX_DIR = "../textures/menuTextures/";

TextureManager::TextureManager()
{
    loadMenuTex();
}


TextureManager::~TextureManager()
{}

TextureManager& TextureManager::getInstance()
{
    if (!_instance)
        _instance.reset(new TextureManager);
    return *_instance.get();
}

void TextureManager::loadMenuTex()
{
    _menuTextures.resize(MenuTextures::NumOfButtons);

    _menuTextures[MenuTextures::BG].loadFromFile(MENU_TEX_DIR + "menuBG.png");

    _menuTextures[MenuTextures::OpenPoly].loadFromFile(MENU_TEX_DIR + "openPolyButton.png");

    _menuTextures[MenuTextures::ClosedPoly].loadFromFile(MENU_TEX_DIR + "closedPolyButton.png");

    _menuTextures[MenuTextures::Square].loadFromFile(MENU_TEX_DIR + "squareButton.png");

    _menuTextures[MenuTextures::EquilateralTriangle].loadFromFile
        (MENU_TEX_DIR + "equilateralTriangleButton.png");

    _menuTextures[MenuTextures::IsoscelesTriangle].loadFromFile
        (MENU_TEX_DIR + "isoscelesTriangleButton.png");

    _menuTextures[MenuTextures::StarOfDavid].loadFromFile(MENU_TEX_DIR + "starOfDavidush.png");

    _menuTextures[MenuTextures::House].loadFromFile(MENU_TEX_DIR + "houseButton.png");

    _menuTextures[MenuTextures::New].loadFromFile(MENU_TEX_DIR + "newButton.png");

    _menuTextures[MenuTextures::Rotate].loadFromFile(MENU_TEX_DIR + "rotateButton.png");

    _menuTextures[MenuTextures::HouseMove].loadFromFile(MENU_TEX_DIR + "houseMoveButton.png");

    _menuTextures[MenuTextures::Delete].loadFromFile(MENU_TEX_DIR + "deleteButton.png");

    _menuTextures[MenuTextures::Shrink].loadFromFile(MENU_TEX_DIR + "shrinkButton.png");

    _menuTextures[MenuTextures::Enlarge].loadFromFile(MENU_TEX_DIR + "enlargeButton.png");

    _menuTextures[MenuTextures::Undo].loadFromFile(MENU_TEX_DIR + "undoButton.png");

    _menuTextures[MenuTextures::Color].loadFromFile(MENU_TEX_DIR + "colorButton.png");

    _menuTextures[MenuTextures::ChooseForward].loadFromFile(MENU_TEX_DIR + "chooseForwardButton.png");

    _menuTextures[MenuTextures::ChooseBackward].loadFromFile(MENU_TEX_DIR + "chooseBackwardButton.png");

    _menuTextures[MenuTextures::SelectSimilars].loadFromFile(MENU_TEX_DIR + "selectSimilarsButton.png");


    

}

const sf::Texture& TextureManager::getMenuTexture(MenuTextures::Tex i) const
{
    return _menuTextures[i];
}
