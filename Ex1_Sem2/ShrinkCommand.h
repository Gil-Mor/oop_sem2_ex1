#pragma once
#include "TransformCommand.h"
class Shape;

class ShrinkCommand :
    public TransformCommand
{
public:
    ShrinkCommand();
    virtual ~ShrinkCommand();

    void action(Shape& shape);
    void undo(Shape& shape);
};

