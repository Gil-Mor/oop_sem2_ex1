#pragma once
#include "Triangle.h"
#include <utility>
using std::pair;

class EquilateralTriangle : public Triangle
{
public:
    EquilateralTriangle(sf::Color& color, sf::Vector2f origin);
    virtual ~EquilateralTriangle();

    void setCenter(sf::Vector2f);

    void setSize(float size);

    virtual float getArea() const;
    virtual float getDiameter() const;

    float getEdgeSize() const;
    sf::Vector2f getAVertex() const;

    void move10P(float pixles);

private:
    float _size, _height;
};

