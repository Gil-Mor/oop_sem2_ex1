#pragma once
class Shape;

class TransformCommand
{
public:
    TransformCommand();
    virtual ~TransformCommand()=0;

    virtual void action(Shape& shape) = 0;
    virtual void undo(Shape& shape) = 0;

};

