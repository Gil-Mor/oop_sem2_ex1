#include "DrawingButton.h"


DrawingButton::DrawingButton(Controller& c, sf::Vector2f& origin,
    const sf::Texture* tex, Controller::State state)
    : Button(c, origin), _shapeState(state)
{
    _shape.setTexture(tex);
}


DrawingButton::~DrawingButton()
{}

void DrawingButton::action()
{
    _controller.setState(_shapeState);
}

