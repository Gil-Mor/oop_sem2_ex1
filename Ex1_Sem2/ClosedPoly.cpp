#include "ClosedPoly.h"

ClosedPoly::ClosedPoly(sf::Color& color, sf::Vector2f origin)
    : Polygon(color, origin)
{
    _vertices.setPrimitiveType(sf::PrimitiveType::LinesStrip);
}


ClosedPoly::~ClosedPoly()
{
}

void ClosedPoly::appendLastVertex(sf::Vector2f origin)
{
    if (_verticesCount != 0)
    {
        _diameter += sqrt(pow(_vertices[0].position.x - _vertices[_verticesCount - 1].position.x, 2)
            + pow(_vertices[0].position.y - _vertices[_verticesCount - 1].position.y, 2));
    }
    _vertices.append(_vertices[0]);
    _vertices[_verticesCount].color = _color;
    ++_verticesCount;
}
