#include "Menu.h"
#include "Controller.h"
#include "TextureManager.h"
#include "StateCommands.h"
#include "windowProporties.h"
#include "UndoButton.h"
#include "EnlargeButton.h"
#include "ShrinkButton.h"
#include "SelectSimilarButton.h"
#include "DrawingButton.h"
#include "RotateButton.h"
#include "NewButton.h"
#include "ColorButton.h"
#include "DeleteButton.h"
#include "HouseMoveButton.h"
#include "ChooseShapeButton.h"

using std::string;

const unsigned int NUM_OF_BUTTONS = 3;
const unsigned int NUM_OF_BUTTONS_IN_ROW = 2;

Menu::Menu(Controller& controller)
    :
    _stateCommands(*(new StateCommands(controller)))
{
    setShape();
    setButtons(controller);
}

void Menu::setShape()
{
    _shape.setSize(MENU_SIZE);
    _shape.setPosition(MENU_ORIGIN);
    _shape.setFillColor(sf::Color::Red);

    _shape.setTexture(&(TextureManager::getInstance().getMenuTexture(MenuTextures::BG)));
}


void Menu::setButtons(Controller& controller)
{

    sf::Vector2f gap(15, 20);
    sf::Vector2f origin = _shape.getPosition() + gap;

    unsigned int alreadyInRow = 0;

    _buttons.push_back(unique_ptr<Button>(new NewButton(controller, origin)));

    advancePosition(origin, gap, alreadyInRow);

    _buttons.push_back(unique_ptr<Button>(new DeleteButton(controller, origin)));

    advancePosition(origin, gap, alreadyInRow);

    _buttons.push_back(unique_ptr<Button>(new UndoButton(controller, origin)));

    advancePosition(origin, gap, alreadyInRow);

    _buttons.push_back(unique_ptr<Button>(new EnlargeButton(controller, origin)));

    advancePosition(origin, gap, alreadyInRow);

    _buttons.push_back(unique_ptr<Button>(new ShrinkButton(controller, origin)));

    advancePosition(origin, gap, alreadyInRow);

    _buttons.push_back(unique_ptr<Button>(new SelectSimilarButton(controller, origin)));

    advancePosition(origin, gap, alreadyInRow);

    _buttons.push_back(unique_ptr<Button>(new ChooseShapeButton
        (controller, origin, Controller::State::ChoosingForward_s)));

    advancePosition(origin, gap, alreadyInRow);

    _buttons.push_back(unique_ptr<Button>(new ChooseShapeButton
        (controller, origin, Controller::State::ChoosingBackward_s)));

    advancePosition(origin, gap, alreadyInRow);

    _buttons.push_back(unique_ptr<Button>(new RotateButton(controller, origin)));

    advancePosition(origin, gap, alreadyInRow);

    _buttons.push_back(unique_ptr<Button>(new ColorButton(controller, origin)));

    advancePosition(origin, gap, alreadyInRow);

    _buttons.push_back(unique_ptr<Button>(new HouseMoveButton(controller, origin)));

    advancePosition(origin, gap, alreadyInRow);

    _buttons.push_back(unique_ptr<Button>(new DrawingButton(controller, origin,
        &TextureManager::getInstance().getMenuTexture(MenuTextures::House),
        Controller::State::DrawingHouse_s)));

    advancePosition(origin, gap, alreadyInRow);

    _buttons.push_back(unique_ptr<Button>(new DrawingButton(controller, origin,
        &TextureManager::getInstance().getMenuTexture(MenuTextures::Square),
        Controller::State::DrawingSquare_s)));

    advancePosition(origin, gap, alreadyInRow);

    _buttons.push_back(unique_ptr<Button>(new DrawingButton(controller, origin,
        &TextureManager::getInstance().getMenuTexture(MenuTextures::ClosedPoly),
        Controller::State::DrawingClosedPoly_s)));

    advancePosition(origin, gap, alreadyInRow);

    _buttons.push_back(unique_ptr<Button>(new DrawingButton(controller, origin,
        &TextureManager::getInstance().getMenuTexture(MenuTextures::OpenPoly),
        Controller::State::DrawingOpenPoly_s)));

    advancePosition(origin, gap, alreadyInRow);

    _buttons.push_back(unique_ptr<Button>(new DrawingButton(controller, origin,
        &TextureManager::getInstance().getMenuTexture(MenuTextures::EquilateralTriangle),
        Controller::State::DrawingEqTriangle_s)));

    advancePosition(origin, gap, alreadyInRow);

    _buttons.push_back(unique_ptr<Button>(new DrawingButton(controller, origin,
        &TextureManager::getInstance().getMenuTexture(MenuTextures::IsoscelesTriangle),
        Controller::State::DrawingIsoTriangle_s)));

    advancePosition(origin, gap, alreadyInRow);

    _buttons.push_back(unique_ptr<Button>(new DrawingButton(controller, origin,
        &TextureManager::getInstance().getMenuTexture(MenuTextures::StarOfDavid),
        Controller::State::DrawingStarOfDavid_s)));

    advancePosition(origin, gap, alreadyInRow);

}

Menu::~Menu()
{}

void Menu::handleEvent(const sf::Event& event, const sf::Vector2f mouse)
{

    switch (event.type)
    {
        case sf::Event::MouseMoved:
        {
            handleMouseMove(mouse);
            break;
        }

        case sf::Event::MouseButtonPressed:
        {
            if (event.mouseButton.button == sf::Mouse::Left)
            {
                handleMousePress(mouse);
            }
            break;
        }
    } 
}


void Menu::handleMouseMove(const sf::Vector2f mouse)
{
    for (unsigned int i = 0; i < _buttons.size(); ++i)
    {
        if (_buttons[i]->mouseOver(mouse))
        {
            _buttons[i]->setInFocus(true);
        }
        else
        {
            _buttons[i]->setInFocus(false);
        }
    }
}


void Menu::handleMousePress(const sf::Vector2f mouse)
{
    for (unsigned int i = 0; i < _buttons.size(); ++i)
    {
        if (_buttons[i]->mouseOver(mouse))
        {
            _buttons[i]->action();
            _buttons[i]->setChosen(true);
        }
        else
            _buttons[i]->setChosen(false);
    }
}


bool Menu::buttonWasPressed() const
{
    for (unsigned int i = 0; i < _buttons.size(); ++i)
    {
        if (_buttons[i]->getChosen())
            return true;
    }
    return false;
}

void Menu::draw(sf::RenderWindow& window) const
{
    window.draw(_shape);

    for (unsigned int i = 0; i < _buttons.size(); ++i)
    {
        _buttons[i]->draw(window);
    }
}



void Menu::advancePosition(sf::Vector2f& pos,
    const sf::Vector2f& gap, unsigned int& alreadyInRow)
{
    pos.x += BUTTON_SIZE.x + gap.x;

    if (++alreadyInRow == NUM_OF_BUTTONS_IN_ROW)
    {
        alreadyInRow = 0;

        pos.y += BUTTON_SIZE.y + gap.y;

        // start x position at the left edge of the menu
        pos.x -= (float)(NUM_OF_BUTTONS_IN_ROW * (BUTTON_SIZE.x + gap.x));
    }
}


bool Menu::mouseOver(const sf::Vector2f& mouse) const
{
    float startX = _shape.getPosition().x;

    float startY = _shape.getPosition().y;

    float endX = (startX + _shape.getSize().x);

    float endY = (startY + _shape.getSize().y);

    return startX < mouse.x && mouse.x < endX
        && startY < mouse.y && mouse.y < endY;
}
