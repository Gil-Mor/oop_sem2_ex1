#include "NewButton.h"
#include "Controller.h"
#include "TextureManager.h"

NewButton::NewButton(Controller& c, sf::Vector2f origin)
    : Button(c, origin)
{
    _shape.setTexture(&(TextureManager::getInstance().getMenuTexture(MenuTextures::New)));
}


NewButton::~NewButton()
{
}

void NewButton::action()
{
    _controller.restart();
}
